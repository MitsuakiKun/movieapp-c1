import fetch from 'node-fetch';

export const getUpcomingMovies = async () => {
    const response = await fetch(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${process.env.TMDB_KEY}&language=en-US&page=1`
    );

    if (!response.ok) {
        throw new Error(response.json().message);
    }

    return await response.json();

};

export const getGenres = async () => {
    const response = await fetch(
        `https://api.themoviedb.org/3/genre/movie/list?api_key=${process.env.TMDB_KEY}&language=en-US&page=1`
    );

    if (!response.ok) {
        throw new Error(response.json().message);
    }

    return await response.json();

};

export const getMovies = async(language) => {
    const response = await fetch(
    `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.TMDB_KEY}&language=${language}&include_adult=false&include_video=false&page=1`
    );
    const responseData = await response.json(); 

    if (!response.ok) {
        throw new Error(response.json().message);
    }

    return await responseData;

};
    
export const getMovie = async({ queryKey }) => {
    console.log(queryKey);
    const [, params] = queryKey;
    const { id ,language} = params;
    const response = await fetch(
        `https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.TMDB_KEY}&language=${language}`
        );

    if (!response.ok) {
        throw new Error(response.json().message);
    }

    return await response.json();
};

export const getMovieImages = async ({ queryKey }) => {
    const [, idPart] = queryKey;
    const { id } = idPart;
    try {
    const response = await fetch(
        `https://api.themoviedb.org/3/movie/${id}/images?api_key=${process.env.TMDB_KEY}`
    );

    if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData.message);
    }

    const imageData = await response.json();
    return imageData;
    } catch (error) {
    console.error('Error:', error);
    throw error;
    }
};

export const getMovieReviews = ( id ) => {

    return fetch(
    `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=${process.env.TMDB_KEY}`
    ).then( (response) => {
    return response.json();
    });
};

export const getSimilarMovies = async({ queryKey }) => {
    console.log(queryKey);
    const [, params] = queryKey;
    const { id ,language} = params;

    const response = await fetch(
        `https://api.themoviedb.org/3/movie/${id}/similar?api_key=${process.env.TMDB_KEY}&language=${language}&include_adult=false&include_video=false&page=1`
        );

    if (!response.ok) {
        throw new Error(response.json().message);
    }

    return await response.json();
};

export const getCredits =async({ queryKey }) => {
    console.log(queryKey);
    const [, params] = queryKey;
    const { id ,language} = params;
    console.log(`MovieDetailPage:${language}`)

    const response = await fetch(
        `https://api.themoviedb.org/3/movie/${id}/credits?api_key=${process.env.TMDB_KEY}&language=${language}`
        );

    if (!response.ok) {
        throw new Error(response.json().message);
    }

    return await response.json();
};




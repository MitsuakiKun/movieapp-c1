import movieModel from './movieModel';
import asyncHandler from 'express-async-handler';
import express from 'express';
import { getUpcomingMovies, getGenres, getMovies, getMovie, getMovieImages, getMovieReviews, getSimilarMovies, getCredits } from '../tmdb-api';

const router = express.Router();

router.get('/', asyncHandler(async (req, res) => {
    const movies = await movieModel.find();
    res.status(200).json(movies);
}));

router.get('/:id', asyncHandler(async (req, res) => {
    const id = parseInt(req.params.id);
    const movie = await movieModel.findByMovieDBId(id);
    if (movie) {
        res.status(200).json(movie);
    } else {
        res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
}));

router.get('/tmdb/upcoming', asyncHandler(async (req, res) => {
    const upcomingMovies = await getUpcomingMovies();
    res.status(200).json(upcomingMovies);
}));

router.get('/tmdb/genres', asyncHandler(async (req, res) => {
    const genres = await getGenres();
    res.status(200).json(genres);
}));

router.get('/tmdb/:language', asyncHandler(async (req, res) => {
    const movies = await getMovies(req.params.language);
    
    res.status(200).json(movies);
}));

router.get('/:id/detail/:language', asyncHandler(async (req, res) => {
        const movie = await getMovie({
            queryKey: [null, { id: req.params.id, language: req.params.language }],
        });
        res.status(200).json(movie);
    } 
));

router.get('/:id/review', asyncHandler(async (req, res) => {
    const reviews = await getMovieReviews(req.params.id);
    res.json(reviews);
}));

router.get('/:id/images', asyncHandler(async (req, res) => {
    const images = await getMovieImages({
        queryKey: [null, { id: req.params.id}],
    });
    res.status(200).json(images);
}));

router.get('/:id/similar/:language', asyncHandler(async (req, res) => {
    const similarMovies = await getSimilarMovies({
        queryKey: [null, { id: req.params.id, language: req.params.language }],
    });
    res.status(200).json(similarMovies);
}));



router.get('/:id/credits/:language', asyncHandler(async (req, res) => {
    const credits = await getCredits({
        queryKey: [null, { id: req.params.id, language: req.params.language }],
    });
    res.status(200).json(credits);
}));

export default router;
import chai from "chai";
import request from "supertest";
import mongoose from "mongoose";
import api from "../../../../index";
import Review from "../../../../api/reviews/reviewModel";

const expect = chai.expect;
let db;

describe("Review Endpoint", () => {
  before(async () => {
    await mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
        await db.dropDatabase();
      } catch (error) {
        console.log(error);
      }
  });

  beforeEach(async () => {
    await Review.deleteMany();
  });

  describe("GET /api/reviews", () => {
    it("should return empty review data", () => {
      return request(api)
        .get("/api/reviews")
        .expect(200);
        });
  });


  describe("GET /api/reviews/:movieId", async() => {
    it("should return a 404 status and a message if no reviews are found for the specified movieId", () => {
      return request(api)
        .get("/api/reviews/0")
        .expect(404)
        .expect({
            success: false,
            message: "No reviews found for the specified movieId",
          });
        });

    it("should return a list of reviews if reviews exist for the specified movieId", async () => {
      return request(api)
      .get("/api/movies/848326/review")
      .set("Accept", "application/json")
      .expect(200);
  });


  });

  describe("POST /api/reviews/:id", () => {
    it("should return a 201 status and a success message for adding a review", async () => {
      const reviewData = {
        movieId: "848326",
        rating: 5,
        review: "Excellent movie",
        author: "Ali",
      };

     
      return request(api)
      .post("/api/reviews/848326")
      .send(reviewData)
      .expect(201)
      .expect({ success: true, msg: "review successfully created." });
    });

    it("should return a 500 status and an error message for internal server error during review creation", async () => {
      const reviewData = {
        movieId: "848326",
      };

      return request(api)
      .post("/api/reviews/848326")
      .send(reviewData)
      .expect(500)
      .expect({ success: false, msg: "Internal server error." });
    });
  });

  describe("DELETE /api/reviews/:id", async() => {
    await request(api).post("/api/reviews/848326").send({
        movieId: "848326", rating: 4, review: "Great movie", author: "mitsu"
    });
    it("should return a 200 status and a success message for deleting a review", async () => {
        return request(api)
        .delete("/api/reviews/848326")
        .expect(200)
        .expect({ success: true, msg: "review successfully deleted." });
    });

    it("should return a 404 status and an error message if the review is not found", async () => {
      // Perform the DELETE request
      return request(api)
      .delete("/api/reviews/0")
      .expect(404)
      .expect({ success: false, msg: "review not found or not associated with the user." });
    });


  });

  describe("DELETE /api/reviews", () => {
    it("should return a 200 status and a success message for deleting all reviews", async () => {
      return request(api)
      .delete("/api/reviews")
      .expect(200)
      .expect({ success: true, msg: "review successfully deleted." });
    });
  });
});


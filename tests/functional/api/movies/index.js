import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";

const expect = chai.expect;
let db;

describe("Movies endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });
  
  beforeEach(async () => {
    try {
      await Movie.deleteMany();
      await Movie.collection.insertMany(movies);
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); 
  });
  describe("GET /api/movies ", () => {
    it("should return 20 movies and a status 200", (done) => {
      request(api)
        .get("/api/movies")
        .set("Accept", "application/json")
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(20);
          done();
        });
    });
  });

  describe("GET /api/movies/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching movie", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", movies[0].title);
          });
      });
    });
  });
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(404);
      });
    });  

    describe("GET /api/movies/tmdb/:language", () => {
      describe("when the language is valid", () => {
        it("should return a status 200", () => {
          return request(api)
            .get(`/api/movies/tmdb/en-US`)
            .set("Accept", "application/json")
            .expect(200);
        });
      });
      describe("when the language is ja-JA", () => {
        it("should return a status 200", () => {
          return request(api)
            .get("/api/movies/tmdb/ja-JA")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200);
        });
      });
    });

    describe("GET /api/movies/tmdb/genres", () => {
      describe("when the endpoint is valid", () => {
        it("should return a status 200", () => {
          return request(api)
            .get(`/api/movies/tmdb/genres`)
            .set("Accept", "application/json")
            .expect(200);
        });
      });
    });

    describe("GET /api/movies/tmdb/upcoming", () => {
      describe("when the language is valid", () => {
        it("should return a status 200", () => {
          return request(api)
            .get(`/api/movies/tmdb/upcoming`)
            .set("Accept", "application/json")
            .expect(200);
        });
      });
      
    });

  });

  describe("GET /api/movies/:id/detail/:language", () => {
    describe("when the id and language are valid", () => {
      it("should return the detailed information of the movie", () => {
        return request(api)
          .get(`/api/movies/848326/detail/en-US`)
          .set("Accept", "application/json")
          .expect(200);
      });
    });
  
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/0/detail/en-US")
          .set("Accept", "application/json")
          .expect(500);
      });
    });
  });
  
  describe("GET /api/movies/:id/review", () => {
    describe("when the id is valid", () => {
      it("should return the reviews of the movie", () => {
        return request(api)
          .get(`/api/movies/848326/review`)
          .set("Accept", "application/json")
          .expect(200);
      });
    });
  
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/0/review")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect({
            success: false,
            status_code: 34,
            status_message: "The resource you requested could not be found.",
          });
      });
    });
  });
  
  describe("GET /api/movies/:id/images", () => {
    describe("when the id is valid", () => {
      it("should return the images of the movie", () => {
        return request(api)
          .get(`/api/movies/848326/images`)
          .set("Accept", "application/json")
          .expect(200);
      });
    });
  
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/0/images")
          .expect(500);
      });
    });
  });
  
  describe("GET /api/movies/:id/similar/:language", () => {
    describe("when the id and language are valid", () => {
      it("should return a list of similar movies", () => {
        return request(api)
          .get(`/api/movies/848326/similar/en-US`)
          .set("Accept", "application/json")
          .expect(200);
      });
    });
  
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/0/similar/en-US")
          .expect(500);
      });
    });
  });
  
  describe("GET /api/movies/:id/credits/:language", () => {
    describe("when the id and language are valid", () => {
      it("should return the credits information of the movie", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}/credits/en-US`)
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body).to.be.an("object");
            // Add more assertions as needed for the credits information
          });
      });
    });
  
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/0/credits/en-US")
          .expect(500);
      });
    });
  });
  



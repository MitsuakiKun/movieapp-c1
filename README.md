# Assignment 2 - Agile Software Practice.

Name: Mitsuaki Okabe

## API endpoints.

+ GET api/movies/:id - get the details of a specific movie.
+ POST api/movies/:id/reviews (Auth) - Add a review for a movie.
+ GET api/movies?page=n&limit=m - Get m list of movies from TMDB's Discover endpoint, starting at page n and limit the list size to m.  
+ POST api/:userName/favourites (Auth) - add a movie to the named user's favourites list.
+ POST /api/users?action=action - Register or authenticate a user - set action to register or login. 
+ POST /api/users/users/{movieid}/favorite - Add favorite 
+ DELETE/api/users/users/{movieid}/favorite - Delete favorites 
+ GET /api/users/favorite - Get favorites
+ POST /api/users/users/{movieid}/mustWatch - Add mustWatch 
+ DELETE /api/users/users/{movieid}/mustWatch - Delete mustWatch
+ GET /api/users/mustWatch - Get mustWatches
+ POST /api/reviews/{movieid} - Add review 
+ DELETE /api/reviews/{movieid} - Delete review
+ GET /api/reviews/{movieid} - Get reviews
+ GET /api/movies/{movieid}/detail/{language} - Get movies detail from TMDB API
+ GET /api/movies/tmdb/geres/{language} - Get genres from TMDB API
+ GET /api/movies/{movieid}/images - Get movie images from TMDB API
+ GET /api/movies/{movieid}/reviews - Get reviews from TMDB API
+ GET /api/movies/tmdb/upcoming/{language} - Get upcoming movies from TMDB API
+ GET /api/movies/{movieid}/similar/{language} - Get similar movies from TMDB API
+ GET /api/movies/{movieid}/credits/{language} - Get credits from TMDB API

## Automated Testing.

~~~
  Users endpoint
    GET /api/users 
     √ should return the 2 users and a status 200 (262ms)
    POST /api/users
      For a register action
        when the payload is correct
          √ should return a 201 status and the confirmation message (335ms)
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (331ms)
      √ should return a 201 status and a success message (539ms)
      √ should return a 200 status and (259ms)
      √ should return a 200 status and a generated token (271ms)
      √ should return a 500 status and an error message for adding a favorite with an invalid user ID (288ms)
      √ should return a 404 status and an error message for deleting a favorite with an invalid user ID (262ms)
      √ should return a 201 status and a success message (268ms)
      √ should return a 200 status and (263ms)
      √ should return a 200 status and a generated token (275ms)
      √ should return a 200 status and an empty list of mustWatches after deletion (266ms)
      √ should return a 500 status and an error message for adding a mustWatch with an invalid user ID (77ms)
      √ should return a 404 status and an error message for deleting a mustWatch with an invalid user ID (259ms)

  Movies endpoint
    GET /api/movies
      √ should return 20 movies and a status 200 (259ms)
    GET /api/movies/:id
      when the id is valid
        √ should return the matching movie (252ms)
    when the id is invalid
      √ should return the NOT found message (251ms)
    GET /api/movies/tmdb/:language
      when the language is valid
        √ should return a status 200 (77ms)
      when the language is ja-JA
        √ should return a status 200 (41ms)
    GET /api/movies/tmdb/genres
      when the endpoint is valid
        √ should return a status 200 (38ms)
    GET /api/movies/tmdb/upcoming
      when the language is valid
        √ should return a status 200

  GET /api/movies/:id/detail/:language
    when the id and language are valid
      √ should return the detailed information of the movie (73ms)
    when the id is invalid
      √ should return the NOT found message (242ms)

  GET /api/movies/:id/review
    when the id is valid
      √ should return the reviews of the movie (48ms)
    when the id is invalid
      √ should return the NOT found message (259ms)

  GET /api/movies/:id/images
    when the id is valid
      √ should return the images of the movie (97ms)
    when the id is invalid
      √ should return the NOT found message (214ms)

  GET /api/movies/:id/similar/:language
    when the id and language are valid
      √ should return a list of similar movies
    when the id is invalid
      √ should return the NOT found message (243ms)

  GET /api/movies/:id/credits/:language
    when the id and language are valid
      √ should return the credits information of the movie (104ms)
    when the id is invalid
      √ should return the NOT found message (374ms)

  Review Endpoint
    GET /api/reviews
      √ should return empty review data (251ms)
    GET /api/reviews/:movieId
      √ should return a 404 status and a message if no reviews are found for the specified movieId (257ms)
      √ should return a list of reviews if reviews exist for the specified movieId (58ms)
    POST /api/reviews/:id
      √ should return a 201 status and a success message for adding a review (265ms)
      √ should return a 500 status and an error message for internal server error during review creation
    DELETE /api/reviews
      √ should return a 200 status and a success message for deleting all reviews (270ms)


  39 passing (34s)

---------------------------------|---------|----------|---------|---------|-------------------
File                             | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
---------------------------------|---------|----------|---------|---------|-------------------
All files                        |    90.8 |    64.71 |   93.36 |   91.71 | 
 movies-api-cicd                 |     100 |      100 |     100 |     100 | 
  index.js                       |     100 |      100 |     100 |     100 | 
 movies-api-cicd/api             |   97.54 |    69.31 |     100 |     100 | 
  tmdb-api.js                    |   97.54 |    69.31 |     100 |     100 | 1-43,58,88,104   
 movies-api-cicd/api/favorites   |     100 |      100 |     100 |     100 | 
  favoriteModel.js               |     100 |      100 |     100 |     100 | 
 movies-api-cicd/api/movies      |     100 |    78.78 |     100 |     100 | 
  index.js                       |     100 |    78.12 |     100 |     100 | 2
  movieModel.js                  |     100 |      100 |     100 |     100 | 
---------------------------------|---------|----------|---------|---------|-------------------
~~~


## Deployments.
Git Lab : https://gitlab.com/MitsuakiKun/movieapp-c1


## Independent Learning (if relevant)
[Istanbul](https://istanbul.js.org/)